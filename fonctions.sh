#!/bin/bash
# TODO
# TODO une fonction qui crée et charge un environnement virtuel
# TODO une fonction qui crée une base de données SQLite3 ==> fait
# TODO une fonction de journalisation de l'installation
# TODO une fonction qui crée une tache planifiée

# ####################################################
#
# Les couleurs
#
# ####################################################


# noir='\e[0;30m'
# gris='\e[1;30m'
rougefonce='\e[0;31m'
rose='\e[1;31m'
vertfonce='\e[0;32m'
# vertclair='\e[1;32m'
orange='\e[0;33m'
# jaune='\e[1;33m'
# bleufonce='\e[0;34m'
# bleuclair='\e[1;34m'
# violetfonce='\e[0;35m'
# violetclair='\e[1;35m'
cyanfonce='\e[0;36m'
# cyanclair='\e[1;36m'
# grisclair='\e[0;37m'
# blanc='\e[1;37m'
neutre='\e[0;m'



# ______ _      _     _                 _____ _   _ _____ 
# |  ___(_)    | |   (_)               |_   _| \ | |_   _|
# | |_   _  ___| |__  _  ___ _ __ ___    | | |  \| | | |  
# |  _| | |/ __| '_ \| |/ _ \ '__/ __|   | | | . ` | | |  
# | |   | | (__| | | | |  __/ |  \__ \  _| |_| |\  |_| |_ 
# \_|   |_|\___|_| |_|_|\___|_|  |___/  \___/\_| \_/\___/ 


# #################################################################
#
# Fonction qui charge un fichier INI et qui en exporte les lignes 
# en autant de variables d'environnement
#
# ################################################################
#
# Paramètre 1        : le fichier ini en question
# Version            : 1.0.1
# Dernière évolution : remplace echo ....par Affiche_Demande_Message

Charge_Fichier_Ini() {
local ConfigFile="$1"

Affiche_Demande_Message "Demande de sourcer le fichier $1"
ls -l "$ConfigFile" > /dev/null 2>&1
LAST=$?
if  [ $LAST -eq 0 ]
then
        Affiche_Demande_Message "Le fichier de configuration $ConfigFile existe "
	while IFS='=' read -r key value; do
    		CLE="${key%"${key##*[![:space:]]}"}"
    		VALEUR="${value#"${value%%[![:space:]]*}"}"
    		# Ignore comments and empty lines
    		if [[ ! $CLE =~ ^(\#|\[)  &&  -n $CLE ]]; then
            		export "$CLE"="$VALEUR"
        	fi
	done < "$ConfigFile"
	Affiche_OK_Message "le fichier $ConfigFile est sourcé ..."
else
	Affiche_KO_Message "Le fichier de configuration $ConfigFile n'existe pas"
	exit 4
fi
}


# ###########################################################################
#
# Fonction de création d'un fichier de configuration ini pour l'installation
#
# ###########################################################################
#
# Paramètre 1        : le nom du fichier INI
# Version            : 1.1
# Dernière évolution : version améliorée pour Hermes Deploy

Estke_Fichier_Config_Existe_Ou_Cree_La ()
{
local ConfigFile=$1
Affiche_Demande_Message "Demande de création du fichier de configuration $ConfigFile ..."
# echo $ConfigFile
#basename $ConfigFile

DirConfigFile=$(dirname "$ConfigFile")
ls -l "$ConfigFile" > /dev/null 2>&1
LAST=$?
if  [ $LAST -eq 0 ]
then
        Affiche_OK_Message "Le fichier de configuration $ConfigFile existe déjà"
else
        Affiche_KO_Message "Le fichier de configuration $ConfigFile n'existe pas"

        cp "$DirConfigFile/Modeles/install.ini.dist" "$ConfigFile"
	LAST=$?
        if  [ $LAST -eq 0 ]
        then
        	Affiche_OK_Message "Création du fichier de configuration $ConfigFile effectuée"
		echo -e " ... Veuillez l'éditer, changer le paramétrage, et relancer de programme"
		exit 55
        else
              	Affiche_KO_Message "Création du fichier de configuration $ConfigFile en erreur"
              	exit 7
        fi
fi
}

# ###################################################
#
# Fonction qui transforme un fichier ini en json
#
# ##################################################
#
# Paramètre 1        : le nom du fichier ini
# Version            : 1.0
# Derniere évolution : version initiale

Transforme_Ini_To_Json()
{
local fichier=$1
Affiche_Demande_Message "Demande de transformation du fichier $fichier en JSON ..."
#file_name=$(ls -l "$fichier" | rev | cut -d '.' -f 2 | rev)
file_name="$(basename "$fichier" .ini)"
#find . -name "$fichier" | rev | cut -d '.' -f 2 |rev)
jc < "$fichier" --ini -p > "$REP_INSTALL_SRC/$REP_ARCHIVE/$file_name.json"
LAST=$?
if  [ $LAST -eq 0 ]
then
	Affiche_OK_Message "Conversion du fichier de configuration $ConfigFile en JSON effectuée"
else
       	Affiche_KO_Message "Création du fichier de configuration $ConfigFile en erreur"
       	#exit 7
fi
#echo $REP_INSTALL_SRC/$REP_ARCHIVE/$file_name.json
}


# ###################################################
#
# Fonction qui cherche une chaine dans un fichier ini pour la transformer en tableau
#
# ##################################################
#
# Paramètre 1        : le nom du fichier
# Paramètre 2        : La chaine recherchée
# Paramètre 3        : Le tableau où seront rangées les informations
# Version            : 1.0
# Dernière évolution : version initiale

Extrait_Tableau_Depuis_Fichier_Ini() {
local Fichier_Ini=$1
    local CHAINE=$2
    local -n TABLEAU=$3

    # Récupération de la ligne contenant la clef CHAINE
    ligne=$(grep "$CHAINE" "$Fichier_Ini")
    #echo "$ligne"
    # Récupération des valeurs entre parenthèses et suppression des guillemets
    values=$(echo "$ligne" | grep -oP '\(.*?\)' | sed 's/[\(\)]//g')
    #echo $values
    # Création du tableau
    IFS=', ' read -r -a "TABLEAU" <<< "$values"
}


# ______                     _        _               
# | ___ \                   | |      (_)              
# | |_/ /___ _ __   ___ _ __| |_ ___  _ _ __ ___  ___ 
# |    // _ \ '_ \ / _ \ '__| __/ _ \| | '__/ _ \/ __|
# | |\ \  __/ |_) |  __/ |  | || (_) | | | |  __/\__ \
# \_| \_\___| .__/ \___|_|   \__\___/|_|_|  \___||___/
#          | |                                       
#          |_|                                       

# ##############################################################
#
# Fonction qui crée des sous-répertoires dans un répertoire
#
# ##############################################################
#
# Paramètre 1        : la liste des sous-répertoires
# Version            : 1.0
# Dernière évolution : version initiale
# OBSOLETE ?
Is_Sub_Folder_Exists()
{
local Liste_Rep_A_Creer=$(env| grep "$1")
local Base=$2
#echo "Sous-repertoires a creer dans $Base : $Liste_Rep_A_Creer"
for i in $Liste_Rep_A_Creer; do
	#echo $i
	#read -r key value 
	#cle=$(echo "$i" | cut -d"=" -f1| sed 's/ *$//g')
	valeur=$(echo "$i" | cut -d"=" -f2| sed 's/ *$//g')
	#cle=`echo $key | sed 's/ *$//g'`
	#valeur=`echo $value | sed 's/ *$//g'`
	#echo $cle "-> " $valeur
	#echo $Base$valeur
	Is_Rep_Exist_Or_Create "$Base$valeur"
        # Ignore comments and empty lines
done
}
# ##############################################################
#
# Fonction qui crée une arborescence applicative
# A partir de variables d'environnement
#
# ##############################################################
#
# Paramètre 1        : la variable d'environnement (en general REP_BASE)
# Paramètre 2        : la valeur de la variable d'environnement
# Version            : 1.0
# Dernière évolution : version initiale
# La fonction Is_Sub_Folder_Exists est surement obsolete

Creation_Arborescence_Applicative() {
    local base_variable=$1
    local base_dir="$2"
    Estke_Repertoire_Existe_Ou_Cree_Le "$base_dir"
    liste_dir=$(env | grep -E "^SOUS_$base_variable")
    #IFS="\n"	
    for ligne in $liste_dir; do
	for line in $liste_dir; do
    		variable=$(echo "$line" | cut -d '=' -f1)
    		sous_rep=$(echo "$line" | cut -d '=' -f2)
    		#echo "Variable: $variable, Sous-répertoire: $sous_rep"
    		Creation_Arborescence_Applicative "$variable" "$base_dir$sous_rep"
	done

    done
}

# ####################################################
#
# Fonction de création de répertoire
#
# ###################################################
# 
# Paramètre 1        : le chemin absolu du répertoire à créer
# Version            : 1.0
# Dernière évolution : version initiale

Estke_Repertoire_Existe_Ou_Cree_Le ()
{
local repertoire=$1
Affiche_Demande_Message "Demande de création du chemin $repertoire"

ls -l "$repertoire" > /dev/null 2>&1
LAST=$?
if  [ $LAST -eq 0 ]
then
        Affiche_OK_Message "Le répertoire $repertoire existe déjà"
else
        Affiche_Demande_Message "Le répertoire $repertoire n'existe pas..."
        sudo mkdir -p "$repertoire"
	LAST=$?
        if  [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "Création du répertoire $repertoire effectuée"
        else
                Affiche_KO_Message "Création du répertoire $repertoire en erreur"
                exit 7
        fi
fi
}


# ______          _                            ___  ______ _____ 
# | ___ \        | |                          / _ \ | ___ \_   _|
# | |_/ /_ _  ___| | ____ _  __ _  ___  ___  / /_\ \| |_/ / | |  
# |  __/ _` |/ __| |/ / _` |/ _` |/ _ \/ __| |  _  ||  __/  | |  
# | | | (_| | (__|   < (_| | (_| |  __/\__ \ | | | || |     | |  
# \_|  \__,_|\___|_|\_\__,_|\__, |\___||___/ \_| |_/\_|     \_/  
#                            __/ |                               
#                           |___/                                


# ############################################################
#
# Fonction de vérification et d installation de packages apt
#
# ############################################################
#
# Paramètre 1        : le nom du package apt à installer
# Version            : 1.0.2
# Dernière évolution : réécriture du test pour savoir si un package est installé ou pas

Estke_Paquet_Apt_Est_Installe_Ou_Installe_Le ()
{
package=$1
Affiche_Demande_Message "Demande d'installation du paquet $package"
if dpkg -s "$package" | grep "Status: install ok installed" >/dev/null 2>&1
then
	Affiche_OK_Message "$package est déjà installé"
else
	Affiche_Demande_Message "$package n'est pas installé, installation en cours..."
	sudo apt-get -y install "$package" >/dev/null 2>&1
	LAST=$?
	if [ $LAST -eq 0 ]
	then
		Affiche_OK_Message "installation $package terminée"
	else
		Affiche_KO_Message "installation $package en erreur, sortie du programme d'installation"
		exit 5
	fi
fi
}


# ______                           _            _                                  
# | ___ \                         | |          | |                                 
# | |_/ / __ _ ___  ___  ___    __| | ___    __| | ___  _ __  _ __   ___  ___  ___ 
# | ___ \/ _` / __|/ _ \/ __|  / _` |/ _ \  / _` |/ _ \| '_ \| '_ \ / _ \/ _ \/ __|
# | |_/ / (_| \__ \  __/\__ \ | (_| |  __/ | (_| | (_) | | | | | | |  __/  __/\__ \
# \____/ \__,_|___/\___||___/  \__,_|\___|  \__,_|\___/|_| |_|_| |_|\___|\___||___/


# #########################################################
#
# Fonction de vérification et de création de base Mariadb
#
# #########################################################
#
# Paramètre 1        : le nom de la base
# Paramètre 2        : le nom du user bdd
# Paramètre 3        : le mot de passe de ce user bdd
# Version            : 1.0
# Dernière évolution : version initiale

Estke_Base_MYSQL_Existe_Ou_Cree_La ()
{
local database=$1
local user=$2
local password=$3
Affiche_Demande_Message "Demande de création de la base de données Mysql $database appartenant à $user"
database_exist=$(sudo mysql -u root -e "show databases" | grep "$database")
if [ "$database_exist" = "$database" ]
then
	Affiche_OK_Message "la base de données $database existe déjà"
else
	Affiche_Demande_Message "Création de la base de données $database en cours..."
	sudo mysql -u root -e "create database $database"
	LAST=$?
	if [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "création de la base de données $database terminée"
        else
                Affiche_KO_Message "création de la base de données $database en erreur, sortie du programme d'installation"
                exit 5
	fi
	sudo  mysql -u root -e "grant all privileges on $database.* TO '$user'@'localhost' identified by '$password';"
	LAST=$?
	if [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "création du user bdd $user terminée"
        else
                Affiche_KO_Message "création de user bdd $user en erreur, sortie du programme d'installation"
                exit 5
        fi
	sudo mysql -u root -e "flush privileges;"
fi
}

# ##########################################
#
# Fonction de création d'un fichier SQLite3
#
# ##########################################
#
# Paramètre 1        : le nom de la future base données
# Version            : 1.0
# Dernière évolution : version initiale

Estke_Base_SQlite3_Existe_Ou_Cree_La(){
local database=$1
Affiche_Demande_Message "Demande de création de la base de données SQLite3 $database"
# Vérification que SQLite3 est bien installé
Estke_Paquet_Apt_Est_Installe_Ou_Installe_Le sqlite3

if [ -e "$database" ]
then
	echo -e " ...$orange [..]$neutre le fichier $database existe déjà"
	grep SQLite "$database" > /dev/null 2>&1
	LAST=$?
	if [ $LAST -eq 1 ]
	then
		Affiche_OK_Message "mais ce n'est pas un fichier SQLite"
                exit 6
	else
		Affiche_OK_Message "et c'est une base SQLite.."
	fi
	
else
	Affiche_Demande_Message "création de la base de données $database en cours..."
	sqlite3 "$database" << EOF > /dev/null 2>&1
CREATE TABLE table_name ( column1 datatype, column2 datatype, column3 datatype );
DROP TABLE table_name;
EOF
	LAST=$?
	if [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "création de la base de données $database terminée"
        else
                Affiche_OK_Message "création de la base de données $database en erreur, sortie du programme d'installation"
                exit 5
        fi
fi
}


# ______      _   _                 
# | ___ \    | | | |                
# | |_/ /   _| |_| |__   ___  _ __  
# |  __/ | | | __| '_ \ / _ \| '_ \ 
# | |  | |_| | |_| | | | (_) | | | |
# \_|   \__, |\__|_| |_|\___/|_| |_|
#        __/ |                      
#       |___/                       


# ###############################################################
#
# Fonction de vérification et installation package python (pip)
#
# ##############################################################
#
# Paramètre 1        : le nom du package python
# Paramètre 2        : S'il faut forcer l'installation
# Version            : 1.0
# Dernière évolution : version initiale

Estke_Paquet_Pip_Est_Installe_Ou_Installe_Le()
{
package=$1
force_install=$2
pip3 show "$package" > /dev/null 2>&1
LAST=$?
if [ $LAST  -eq 0 ];
then
        Affiche_OK_Message "Python ==> $package est déjà installé"
else
        Affiche_Demande_Message "Python ==> $package n'est pas installé, installation en cours..."
        if [ -z "$force_install" ]
        then
            pip3 install "$package" >/dev/null 2>&1
        else
            pip3 install jinja2-cli --break-system-packages
        fi
        LAST=$?
        if [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "Python ==> installation $package terminée"
        else
                Affiche_KO_Message "Python ==> installation $package en erreur, sortie du programme d'installation"
                #exit 5
        fi
fi
}


#  _____           _                     _ 
# /  ___|         | |                   | |
# \ `--. _   _ ___| |_ ___ _ __ ___   __| |
#  `--. \ | | / __| __/ _ \ '_ ` _ \ / _` |
# /\__/ / |_| \__ \ ||  __/ | | | | | (_| |
# \____/ \__, |___/\__\___|_| |_| |_|\__,_|
#        __/ |                            
#       |___/                             


# ########################################################################
#
# Fonction de création de service (systemctl)
#
# ########################################################################
# On créé un service qui par défaut s\'appelle NOM_APPLICATION.service
# et on relance
#
# Paramètre 1        : le nom du service
# Paramètre 2        : le propriétaire du service
# Paramètre 3        : Type Flask-Waitress
# Version            : 1.0.1
# Dernière évolution : Correction syntaxe (Shellcheck)

Estke_Service_Existe_Ou_Create_Le(){
nom_application=$1
nom_application_service=$1".service"
nom_proprietaire_service=$2
service_flask_waitress=$3
# echo $1
#set -x
Affiche_Demande_Message "Demande de création du service $nom_application_service pour systemd"
if [ -f /etc/systemd/system/"$nom_application_service" ]
then
	Affiche_OK_Message "Le service $nom_application_service existe déjà "
else
	Affiche_Demande_Message "Création du fichier de service $nom_application_service"
if [ -z "$service_flask_waitress" ]
then
	~/.local/bin/jinja2 --format json "$REP_INSTALL_SRC/$REP_ARCHIVE/Modeles/modele_service.j2" "$REP_INSTALL_SRC/$REP_ARCHIVE/install-$Application.json" -D nom_application="$nom_application" > "$REP_INSTALL_SRC/$REP_ARCHIVE/$nom_application_service"
	#Liste_Services="${Liste_Services} $nom_application_service"
	#echo $Liste_Services

else
        ~/.local/bin/jinja2 --format json "$REP_INSTALL_SRC/$REP_ARCHIVE/Modeles/modele_service_flask.j2" "$REP_INSTALL_SRC/$REP_ARCHIVE/install-$Application.json" -D nom_application="$nom_application" > "$REP_INSTALL_SRC/$REP_ARCHIVE/$nom_application_service"
	#Liste_Services="${Liste_Services} $nom_application_service"
fi
	LAST=$?
        if  [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "Création du fichier $nom_application_service effectuée"
        else
                Affiche_KO_Message "La création du fichier $nom_application_service a échoué"
                exit 11

        fi
	Affiche_Demande_Message "Copie du fichier service $nom_application_service dans /etc/systemd/system"
	sudo mv "$nom_application_service" /etc/systemd/system/
	LAST=$?
        if  [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "Copie du fichier $nom_application_service effectuée dans /etc/systemd/system"
        else
                Affiche_KO_Message "La copie du fichier $nom_application_service dans /etc/systemd/system a échoué"
                exit 11
        fi
	Affiche_Demande_Message "Rechargement de la configuration de systemctl"
	sudo systemctl daemon-reload
        LAST=$?
        if  [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "Rechargement de la configuration de systemctl"
        else
                Affiche_KO_Message "Le rechargement de la configuration de systemctl a échoué"
                exit 11
        fi
	Affiche_Demande_Message "Activation du service $nom_application_service"
        sudo systemctl enable "$nom_application_service"
        LAST=$?
        if  [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "Activation du service $nom_application_service effectuée"
        else
                Affiche_KO_Message "L'activation du service $nom_application_service a échoué"
                exit 11
        fi
fi
}

# #################################################################
#
# Fonction de vérification de démarrage d'un service Linux
#
# ################################################################
#
# Paramètre 1        : le nom du service
# Version            : 1.0
# Dernière évolution : version initiale

Estke_Service_OK_Ou_Demarre_Le(){
nom_service=$1
Affiche_Demande_Message "Demande de démarrage du service $nom_service"
sudo systemctl status "$nom_service" >/dev/null 2>&1
LAST=$?
if [ $LAST -eq 0 ];
then
	Affiche_OK_Message "Le service $nom_service est démarré.. "
elif [ $LAST -eq 3 ]
then
	Affiche_Demande_Message "Le service $nom_service existe mais n'est pas démarré.. "
	#echo -e " ... Tentative de démarrage ..."
	sudo systemctl start "$nom_service" >/dev/null 2>&1
	sleep 5
	sudo systemctl is-active "$nom_service" >/dev/null 2>&1
	LAST=$?
	if [ $LAST -eq 0 ]
	then
		Affiche_OK_Message "démarrage du service $nom_service effectué"
	else
		Affiche_KO_Message "Le démarrage du service $nom_service à échoué"
		#exit 64
	fi
elif [ $LAST -eq 4 ]
then
	Affiche_OK_Message "le service $nom_service n'existe pas"
	exit 65
fi
}

# #################################################################
#
# Fonction de vérification d'arrêt d'un service Linux
#
# ################################################################
#
# Paramètre 1        : le nom du service
# Version            : 1.0
# Dernière évolution : version initiale

Eske_Service_Arrete_Ou_Stop_le(){
nom_service=$1
Affiche_Demande_Message "Demande d'arrêt du service $nom_service"
sudo systemctl status "$nom_service"
LAST=$?
if [ $LAST -eq 3 ];
then
	Affiche_OK_Message "Le service $nom_service est arrêté.. "
elif [ $LAST -eq 3 ]
then
	Affiche_Demande_Message "Le service $nom_service existe mais n'est pas arrêté.. "
	#echo -e " ... Tentative de démarrage ..."
	sudo systemctl stop "$nom_service"
        Affiche_Attente_Message test 300 &
        while [ "$(sudo systemctl is-active "$nom_service")" = "active" ]; do
        	# Ensuite, faites votre traitement ici
        	# Par exemple, un traitement fictif de 5 secondes
        	sleep 1

        	# Arrêter l'affichage de l'animation de chargement
        	done
        kill $!
	sleep 5
	sudo systemctl is-active "$nom_service"
	LAST=$?
	if [ $LAST -eq 0 ]
	then
		Affiche_OK_Message "démarrage du service $nom_service effectué"
	else
		Affiche_OK_Message "Le démarrage du service $nom_service à échoué"
		exit 64
	fi
elif [ $LAST -eq 4 ]
then
	Affiche_KO_Message "le service $nom_service n'existe pas"
	exit 65
fi
}


#  _   _                 _     _                  
# | | | |               | |   (_)                 
# | | | |___  ___ _ __  | |    _ _ __  _   ___  __
# | | | / __|/ _ \ '__| | |   | | '_ \| | | \ \/ /
# | |_| \__ \  __/ |    | |___| | | | | |_| |>  < 
#  \___/|___/\___|_|    \_____/_|_| |_|\__,_/_/\_\


# ##############################################################
#
# Fonction d'ajout de groupe secondaire à un utilisateur linux
#
# ##############################################################
# 
# Paramètre 1        : utilisateur
# Paramètre 2        : le nom du groupe secondaire
# Version            : 1.0
# Dernière évolution : version initiale

Estke_Utilisateur_Dans_Groupe_Ou_Ajoute_Le(){
local utilisateur=$1
local groupe=$2
Affiche_Demande_Message "Demande d'ajout de l utilisateur $utilisateur dans le groupe $groupe"
grep "$utilisateur" /etc/passwd > /dev/null 2>&1
LAST=$?
if [ $LAST -ne 0 ];
then
	Affiche_KO_Message "cet utilisateur $utilisateur n'existe pas!!!"
	#exit 55
fi
grep "$groupe" /etc/group > /dev/null 2>&1
LAST=$?
if [ $LAST -ne 0 ];
then
	Affiche_KO_Message "ce groupe $groupe n'existe pas!!!"
	#exit 56
fi
groups "$utilisateur" | grep "$groupe" > /dev/null 2>&1
LAST=$?
if [ $LAST  -eq 0 ];
then
        Affiche_OK_Message "L'utilisateur $utilisateur est déjà membre du groupe $groupe"
else
        Affiche_Demande_Message "Ajout de l'utilisateur $utilisateur dans le groupe $groupe..."
	sudo usermod -aG "$groupe" "$utilisateur" > /dev/null 2>&1
	LAST=$?
        if [ $LAST -eq 0 ]
        then
                Affiche_OK_Message "L'utilisateur $utilisateur est maintenant membre du groupe $groupe"
        else
                Affiche_OK_Message "L'ajout de l'utilisateur $utilisateur au groupe $groupe a échoué"
                #exit 5
        fi
fi
}

# #########################################
#
# Fonction de création d'un utilisateur
#
# #########################################
#
# Paramètre 1        : le nom de l'utilisateur
# Paramètre 2        : le dossier Home
# Version            : 1.1
# Dernière évolution : Ajout option repertoire utilisateur

Estke_Utilisateur_Existe_Ou_Cree_Le(){
local utilisateur=$1
local rep_home=$2
Affiche_Demande_Message "Demande de création de l'utilisateur Linux $utilisateur ..."
grep "$utilisateur" /etc/passwd > /dev/null 2>&1
LAST=$?
if [ $LAST -eq 0 ];then
	Affiche_OK_Message "cet utilisateur $utilisateur existe déjà"
elif [ ! -e "$rep_home" ];then
	Affiche_KO_Message "Le repertoire $rep_home n'existe pas"
	exit 55
else
	sudo useradd "$utilisateur" -d "$rep_home" -s "/bin/bash"> /dev/null 2>&1
	LAST=$?
	if [ $LAST  -eq 0 ];
	then
        	Affiche_OK_Message "Création de l'utilisateur $utilisateur effectuée "
	else
        	Affiche_KO_Message "La création de l'utilisateur $utilisateur a échouée!"
                exit 15
	fi
fi
}


#   ___   __  __ _      _           ___  ___                               
#  / _ \ / _|/ _(_)    | |          |  \/  |                               
# / /_\ \ |_| |_ _  ___| |__   ___  | .  . | ___  ___ ___  __ _  __ _  ___ 
# |  _  |  _|  _| |/ __| '_ \ / _ \ | |\/| |/ _ \/ __/ __|/ _` |/ _` |/ _ \
# | | | | | | | | | (__| | | |  __/ | |  | |  __/\__ \__ \ (_| | (_| |  __/
# \_| |_/_| |_| |_|\___|_| |_|\___| \_|  |_/\___||___/___/\__,_|\__, |\___|
#                                                                __/ |     
#                                                               |___/      

# #####################################################
#
# Fonction Affiche Attente message
#
# #####################################################
#
# Paramètre 1        : le message
# Paramètre 2        : durée de la temporisation, en seconde
# Version            : 1.0
# Dernière évolution : version initiale

# Fonction pour afficher l'animation de chargement
Affiche_Attente_Message() {
message=$1
temporisation=$2
local barre_charge="|/-\\"
#$((etape + 1))
date_debut="$(date +%s)"
date_fin=$(("$date_debut" + "$temporisation"))
echo "$date_debut"
echo "$date_fin"

while [ "$(date +%s)" -lt "$date_fin" ]; do
        for char in ${barre_charge}; do
        echo -ne "\rTraitement en cours... ${char}"
        sleep 0.1
        done
done
}

# #####################################################
#
# Fonction Affiche OK le résultat d'une action
#
# #####################################################
#
# Paramètre 1        : le message
# Version            : 1.0
# Dernière évolution : version initiale

Affiche_OK_Message(){
local message=$1
echo -e " ...$vertfonce [OK]$neutre $1"
}

# #####################################################
#
# Fonction Affiche KO le résultat d'une action
#
# #####################################################
#
# Paramètre 1        : le message
# Version            : 1.0
# Dernière évolution : version initiale

Affiche_KO_Message(){
local message=$1
echo -e " ...$rougefonce [KO]$neutre $1"
}

# #####################################################
#
# Fonction Affiche demande d'une action
#
# #####################################################
#
# Paramètre 1        : le message
# Version            : 1.0
# Dernière évolution : version initiale

Affiche_Demande_Message(){
local message=$1
echo -e " ...$cyanfonce [..]$neutre $1"
}

# #####################################################
#
# Fonction Affiche Message Avertissement
#
# #####################################################
#
# Paramètre 1        : le message
# Version            : 1.0
# Dernière évolution : version initiale

Affiche_Avertissement_Message(){
local message="$1"
# Définit la couleur jaune en gras
YELLOW_BOLD=$(tput setaf 3)$(tput bold)
RESET=$(tput sgr0)
echo -e "\n$rougefonce [ $YELLOW_BOLD\xE2\x9A\xA0 $RESET $rougefonce ] $neutre $message\n"
}

# #####################################################
#
# Fonction Affiche Message Etape
#
# #####################################################
#
# Paramètre 1        : le message
# Version            : 1.0
# Dernière évolution : version initiale

Affiche_Etape_Message(){
local etape="$1"
local message="$2"
echo -e "\n$vertfonce [ ETAPE $etape ] $neutre $message\n"
}

# #####################################################
#
# Fonction Affiche Message Figlet
#
# #####################################################
#
# Paramètre 1        : Numéro de la partie dans le programme d'installation
# Paramètre 2        : le message
# Version            : 1.01
# Dernière évolution : parametre -w passé de 5 a 80 pour figlet

Affiche_Figlet_Message(){
local partie="$1"
local message="$2"
echo -e "$rose"
figlet -w 80 -t "Partie $partie : $message"
echo -e "$neutre"
}


